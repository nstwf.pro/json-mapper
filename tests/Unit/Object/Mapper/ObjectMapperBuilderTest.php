<?php


namespace Nstwf\JsonMapper\Unit\Object\Mapper;


use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapperBuilder;
use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;
use PHPUnit\Framework\TestCase;


class ObjectMapperBuilderTest extends TestCase
{
    public function testBuild()
    {
        $builder = new ObjectMapperBuilder();
        $builder->withPropertyMapper(new PropertyMapper());

        $this->assertInstanceOf(ObjectMapper::class, $builder->build());
    }
}