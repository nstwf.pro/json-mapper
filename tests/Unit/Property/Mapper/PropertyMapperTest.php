<?php


namespace Nstwf\JsonMapper\Unit\Property\Mapper;


use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\OperationResult\OperationResult;
use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;
use Nstwf\JsonMapper\Registry\ClassFactoryRegistry;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;
use Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum;
use Nstwf\JsonMapper\Unit\Implementation\Enum\StringEnum;
use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleObject;
use Nstwf\JsonMapper\Unit\Implementation\SimpleAnotherObject;
use Nstwf\JsonMapper\Unit\Implementation\Uuid;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;


class PropertyMapperTest extends TestCase
{
    private JsonMapper $jsonMapper;

    protected function setUp(): void
    {
        $this->jsonMapper = $this->createMock(JsonMapper::class);

        parent::setUp();
    }

    public function testMapScalarType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 3;
        $property = new TypeMap(
            new TypeDescriptor('int', false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(3, $expected->value());
    }

    public function testMapNotScalarValueToScalarType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = (object)[1];
        $property = new TypeMap(
            new TypeDescriptor('int', false),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Value type (object) is not equals defined type (int)', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapMixedType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 'value';
        $property = new TypeMap(
            new TypeDescriptor('mixed', false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('value', $expected->value());
    }

    public function testMapUndefinedTypeReturnReceivedValue()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = ['value214', 241.24];
        $property = new TypeMap();

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(['value214', 241.24], $expected->value());
    }

    public function testMapArrayOfScalarType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = [1, 2, 3];
        $property = new TypeMap(
            new TypeDescriptor('int', true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([1, 2, 3], $expected->value());
    }

    public function testMapEnum()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = '1';
        $property = new TypeMap(
            new TypeDescriptor(IntEnum::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(IntEnum::ONE, $expected->value());
    }

    public function testMapArrayOfEnum()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = ['1', '2'];
        $property = new TypeMap(
            new TypeDescriptor(IntEnum::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([IntEnum::ONE, IntEnum::TWO], $expected->value());
    }

    public function testMapInvalidEnumValue()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = [1, 2, 4];
        $property = new TypeMap(
            new TypeDescriptor(IntEnum::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Cannot map value (4) to enum (Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum)', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapOneInvalidEnumValueOfArray()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 5;
        $property = new TypeMap(
            new TypeDescriptor(IntEnum::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Cannot map value (5) to enum (Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum)', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapObject()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $this->jsonMapper->method('mapObject')
            ->with((object)['property' => 1], SimpleObject::class)
            ->willReturn(OperationResult::success(new SimpleObject(1)));

        $value = (object)['property' => 1];
        $property = new TypeMap(
            new TypeDescriptor(SimpleObject::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(new SimpleObject(1), $expected->value());
    }

    public function testMapArrayOfObject()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $this->jsonMapper->method('mapObject')
            ->withConsecutive(
                [(object)['property' => 1], SimpleObject::class],
                [(object)['property' => 5], SimpleObject::class]
            )
            ->willReturnOnConsecutiveCalls(
                OperationResult::success(new SimpleObject(1)),
                OperationResult::success(new SimpleObject(5))
            );

        $value = [(object)['property' => 1], (object)['property' => 5]];
        $property = new TypeMap(
            new TypeDescriptor(SimpleObject::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([new SimpleObject(1), new SimpleObject(5)], $expected->value());
    }

    public function testMapArrayOfObjectWithOneError()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $this->jsonMapper->method('mapObject')
            ->withConsecutive(
                [(object)['property' => 1], SimpleObject::class],
                [(object)['property' => 5], SimpleObject::class]
            )
            ->willReturnOnConsecutiveCalls(
                OperationResult::success(new SimpleObject(1)),
                OperationResult::error('CUSTOM_MESSAGE')
            );

        $value = [(object)['property' => 1], (object)['property' => 5]];
        $property = new TypeMap(
            new TypeDescriptor(SimpleObject::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('CUSTOM_MESSAGE', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapClassUsingRegistry()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(Uuid::class, fn(string $data) => new Uuid($data));

        $value = '24524-24242-242';
        $property = new TypeMap(
            new TypeDescriptor(Uuid::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(new Uuid('24524-24242-242'), $expected->value());
    }

    public function testMapClassUsingRegistryWillReturnDifferentTypeThanRegistry()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(Uuid::class, fn(string $data) => new \stdClass());

        $value = '24524-24242-242';
        $property = new TypeMap(
            new TypeDescriptor(Uuid::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Registry created object type - object, but defined - Nstwf\JsonMapper\Unit\Implementation\Uuid', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapClassUsingRegistryWillNotImplementsDefinedInterface()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(CacheInterface::class, fn(string $data) => new \stdClass());

        $value = '24524-24242-242';
        $property = new TypeMap(
            new TypeDescriptor(CacheInterface::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Registry created object type (stdClass), must implements defined interface (Psr\SimpleCache\CacheInterface)', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapArrayOfClassesUsingRegistry()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(Uuid::class, fn(string $data) => new Uuid($data));

        $value = ['123-25', '86-634', '1673-347347'];
        $property = new TypeMap(
            new TypeDescriptor(Uuid::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([new Uuid('123-25'), new Uuid('86-634'), new Uuid('1673-347347')], $expected->value());
    }

    public function testMapArrayOfClassesUsingRegistryWithOneExceptionWillReturnError()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(Uuid::class, fn() => throw new \Exception('CUSTOM_ERROR'));

        $value = ['123-25',];
        $property = new TypeMap(
            new TypeDescriptor(Uuid::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Can not create object (Nstwf\JsonMapper\Unit\Implementation\Uuid) from registry with data ("123-25"). Error: CUSTOM_ERROR', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapUnknownType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 'value';

        $property = new TypeMap(
            new TypeDescriptor('Unknown', false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertTrue($expected->isError());
        $this->assertEquals('Can not map data ("value") to type (Unknown)', $expected->errorMessage());
    }

    public function testMapUnionScalarType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 'value';

        $property = new TypeMap(
            new TypeDescriptor('int', false),
            new TypeDescriptor('string', false),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('value', $expected->value());
    }

    public function testMapUnionArrayOfScalarType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = ['value1', 'value2'];
        $property = new TypeMap(
            new TypeDescriptor('int', true),
            new TypeDescriptor('string', true),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(['value1', 'value2'], $expected->value());
    }

    public function testMapUnionScalarTypeWithoutExistingTypeWillReturnError()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 1.55;
        $property = new TypeMap(
            new TypeDescriptor('int', false),
            new TypeDescriptor('string', false),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertTrue($expected->isError());
    }

    public function testMapUnionArrayOfScalarTypeWithSeveralValueTypesWillReturnError()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = [1, 4, 5, 1.55];
        $property = new TypeMap(
            new TypeDescriptor('int', true),
            new TypeDescriptor('string', true),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Can not map data ([1,4,5,1.55]) to union type (array of int, array of string)', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapUnionEnum()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 1;
        $property = new TypeMap(
            new TypeDescriptor(IntEnum::class, false),
            new TypeDescriptor(StringEnum::class, false)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(IntEnum::ONE, $expected->value());
    }

    public function testMapUnionArrayOfEnum()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = [1, 2];
        $property = new TypeMap(
            new TypeDescriptor(IntEnum::class, true),
            new TypeDescriptor(StringEnum::class, true)
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([IntEnum::ONE, IntEnum::TWO], $expected->value());
    }

    public function testMapUnionClass()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $this->jsonMapper->method('mapObject')
            ->with((object)['property' => 1], SimpleObject::class)
            ->willReturn(OperationResult::success(new SimpleObject(1)));

        $value = (object)['property' => 1];
        $property = new TypeMap(
            new TypeDescriptor(SimpleObject::class, false),
            new TypeDescriptor(SimpleAnotherObject::class, false),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(new SimpleObject(1), $expected->value());
    }

    public function testMapUnionArrayOfObject()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $this->jsonMapper->method('mapObject')
            ->withConsecutive(
                [(object)['property' => 1], SimpleObject::class],
                [(object)['property' => 5], SimpleObject::class]
            )
            ->willReturnOnConsecutiveCalls(
                OperationResult::success(new SimpleObject(1)),
                OperationResult::success(new SimpleObject(5))
            );

        $value = [(object)['property' => 1], (object)['property' => 5]];
        $property = new TypeMap(
            new TypeDescriptor(SimpleObject::class, true),
            new TypeDescriptor(SimpleAnotherObject::class, true),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([new SimpleObject(1), new SimpleObject(5)], $expected->value());
    }

    public function testMapUnionClassUsingRegistry()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(Uuid::class, fn(string $data) => new Uuid($data));

        $value = '24524-24242-242';
        $property = new TypeMap(
            new TypeDescriptor('int', false),
            new TypeDescriptor(Uuid::class, false),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals(new Uuid('24524-24242-242'), $expected->value());
    }

    public function testMapUnionArrayOfClassesUsingRegistry()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $objectFactoryRegistry->add(Uuid::class, fn(string $data) => new Uuid($data));

        $value = ['123-25', '86-634', '1673-347347'];
        $property = new TypeMap(
            new TypeDescriptor('int', true),
            new TypeDescriptor(Uuid::class, true),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals([new Uuid('123-25'), new Uuid('86-634'), new Uuid('1673-347347')], $expected->value());
    }

    public function testMapNotArrayValueToArrayType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = 1;
        $property = new TypeMap(
            new TypeDescriptor('int', true),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Defined type is array, but value type is not array', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

    public function testMapArrayValueToNotArrayType()
    {
        // Arrange
        $objectFactoryRegistry = new ClassFactoryRegistry();
        $propertyMapper = new PropertyMapper($objectFactoryRegistry);

        $value = [1];
        $property = new TypeMap(
            new TypeDescriptor('int', false),
        );

        // Act
        $expected = $propertyMapper->mapValue($this->jsonMapper, $property, $value);

        // Assert
        $this->assertEquals('Defined type is not array, but value type is array', $expected->errorMessage());
        $this->assertTrue($expected->isError());
    }

}