<?php


namespace Nstwf\JsonMapper\Unit\JsonMapper;


use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\JsonMapper\JsonMapperFactory;
use PHPUnit\Framework\TestCase;


class JsonMapperFactoryTest extends TestCase
{
    public function testDefault()
    {
        $factory = new JsonMapperFactory();

        $this->assertInstanceOf(JsonMapper::class, $factory->create());
    }
}