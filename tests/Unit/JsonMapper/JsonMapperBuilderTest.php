<?php


namespace Nstwf\JsonMapper\Unit\JsonMapper;


use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\JsonMapper\JsonMapperBuilder;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;
use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;
use Nstwf\JsonMapper\Registry\ClassFactoryRegistry;
use Nstwf\JsonMapper\Unit\Implementation\Inspector\EmptyInspector;
use PHPUnit\Framework\TestCase;


class JsonMapperBuilderTest extends TestCase
{
    public function testBuild()
    {
        $builder = new JsonMapperBuilder();

        $builder->withInspector(new EmptyInspector())
            ->withObjectMapper(new ObjectMapper(new PropertyMapper(new ClassFactoryRegistry())));

        $this->assertInstanceOf(JsonMapper::class, $builder->build());
    }
}