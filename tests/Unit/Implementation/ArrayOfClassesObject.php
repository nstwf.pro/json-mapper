<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleObject;
use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleOtherObject;


final class ArrayOfClassesObject
{
    /** @var SimpleObject[] */
    private array $simpleObjects;
    /** @var SimpleOtherObject[] */
    private array $simpleOtherObjects;
    /** @var SimpleAnotherObject[] */
    private array $simpleAnotherObjects;

    /**
     * @param SimpleObject[]        $simpleObjects
     * @param SimpleOtherObject[]   $simpleOtherObjects
     * @param SimpleAnotherObject[] $simpleAnotherObjects
     */
    public function __construct(array $simpleObjects, array $simpleOtherObjects, array $simpleAnotherObjects)
    {
        $this->simpleObjects = $simpleObjects;
        $this->simpleOtherObjects = $simpleOtherObjects;
        $this->simpleAnotherObjects = $simpleAnotherObjects;
    }

    /**
     * @param SimpleObject[] $simpleObjects
     */
    public function setSimpleObjects(array $simpleObjects): void
    {
        $this->simpleObjects = $simpleObjects;
    }

    /**
     * @param SimpleOtherObject[] $simpleOtherObjects
     */
    public function setSimpleOtherObjects(array $simpleOtherObjects): void
    {
        $this->simpleOtherObjects = $simpleOtherObjects;
    }

    /**
     * @param SimpleAnotherObject[] $simpleAnotherObjects
     */
    public function setSimpleAnotherObjects(array $simpleAnotherObjects): void
    {
        $this->simpleAnotherObjects = $simpleAnotherObjects;
    }

    public function getSimpleObjects(): array
    {
        return $this->simpleObjects;
    }

    public function getSimpleOtherObjects(): array
    {
        return $this->simpleOtherObjects;
    }

    public function getSimpleAnotherObjects(): array
    {
        return $this->simpleAnotherObjects;
    }
}