<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class UnionScalarTypesObject
{
    /** @var string|int|bool|float */
    private string|int|bool|float $union;
    /** @var string|int|bool|array|float */
    private string|int|bool|array|float $unionWithArray;
    /** @var string|int|bool|float|null */
    private string|int|bool|null|float $nullableUnion;
    /** @var string[]|int[]|bool[]|float[] */
    private array $unionArray;

    /**
     * @param int|float|string|bool         $union
     * @param int|float|string|bool|array   $unionWithArray
     * @param int|float|string|bool|null    $nullableUnion
     * @param string[]|int[]|bool[]|float[] $unionArray
     */
    public function __construct(
        int|float|string|bool $union,
        int|float|string|bool|array $unionWithArray,
        int|float|string|bool|null $nullableUnion,
        array $unionArray
    ) {
        $this->union = $union;
        $this->unionWithArray = $unionWithArray;
        $this->nullableUnion = $nullableUnion;
        $this->unionArray = $unionArray;
    }

    /**
     * @param float|bool|int|string $union
     */
    public function setUnion(float|bool|int|string $union): void
    {
        $this->union = $union;
    }

    /**
     * @param float|int|bool|array|string $unionWithArray
     */
    public function setUnionWithArray(float|int|bool|array|string $unionWithArray): void
    {
        $this->unionWithArray = $unionWithArray;
    }

    /**
     * @param float|bool|int|string|null $nullableUnion
     */
    public function setNullableUnion(float|bool|int|string|null $nullableUnion): void
    {
        $this->nullableUnion = $nullableUnion;
    }

    /**
     * @param string[]|int[]|bool[]|float[] $unionArray
     */
    public function setUnionArray(array $unionArray): void
    {
        $this->unionArray = $unionArray;
    }

    public function getUnionArray(): array
    {
        return $this->unionArray;
    }

    public function getUnion(): float|bool|int|string
    {
        return $this->union;
    }

    public function getUnionWithArray(): float|int|bool|array|string
    {
        return $this->unionWithArray;
    }

    public function getNullableUnion(): float|bool|int|string|null
    {
        return $this->nullableUnion;
    }
}