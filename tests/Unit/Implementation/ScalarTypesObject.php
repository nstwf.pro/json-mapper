<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class ScalarTypesObject
{
    /** @var int */
    private int $intProperty;
    /** @var string */
    private string $stringProperty;
    /** @var float */
    private float $floatProperty;
    /** @var bool */
    private bool $boolProperty;
    /** @var array */
    private array $arrayProperty;
    /** @var int|null */
    private ?int $nullableIntProperty;

    /**
     * @param int      $intProperty
     * @param string   $stringProperty
     * @param float    $floatProperty
     * @param bool     $boolProperty
     * @param array    $arrayProperty
     * @param int|null $nullableIntProperty
     */
    public function __construct(
        int $intProperty,
        string $stringProperty,
        float $floatProperty,
        bool $boolProperty,
        array $arrayProperty,
        ?int $nullableIntProperty,
    ) {
        $this->intProperty = $intProperty;
        $this->stringProperty = $stringProperty;
        $this->floatProperty = $floatProperty;
        $this->boolProperty = $boolProperty;
        $this->arrayProperty = $arrayProperty;
        $this->nullableIntProperty = $nullableIntProperty;
    }

    /**
     * @param int $intProperty
     */
    public function setIntProperty(int $intProperty): void
    {
        $this->intProperty = $intProperty;
    }

    /**
     * @param string $stringProperty
     */
    public function setStringProperty(string $stringProperty): void
    {
        $this->stringProperty = $stringProperty;
    }

    /**
     * @param float $floatProperty
     */
    public function setFloatProperty(float $floatProperty): void
    {
        $this->floatProperty = $floatProperty;
    }

    /**
     * @param bool $boolProperty
     */
    public function setBoolProperty(bool $boolProperty): void
    {
        $this->boolProperty = $boolProperty;
    }

    /**
     * @param array $arrayProperty
     */
    public function setArrayProperty(array $arrayProperty): void
    {
        $this->arrayProperty = $arrayProperty;
    }

    /**
     * @param int|null $nullableIntProperty
     */
    public function setNullableIntProperty(?int $nullableIntProperty): void
    {
        $this->nullableIntProperty = $nullableIntProperty;
    }

    public function getIntProperty(): int
    {
        return $this->intProperty;
    }

    public function getStringProperty(): string
    {
        return $this->stringProperty;
    }

    public function getFloatProperty(): float
    {
        return $this->floatProperty;
    }

    public function isBoolProperty(): bool
    {
        return $this->boolProperty;
    }

    public function getArrayProperty(): array
    {
        return $this->arrayProperty;
    }

    public function getNullableIntProperty(): ?int
    {
        return $this->nullableIntProperty;
    }
}