<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation\Enum;


enum StringEnum: string
{
    case NAME = 'name';
    case SURNAME = 'surname';
}