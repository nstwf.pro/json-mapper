<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleObject;
use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleOtherObject;


final class NestedUnionSimpleObject
{
    /** @var SimpleOtherObject|SimpleObject */
    private SimpleOtherObject|SimpleObject $simpleObject;
    /** @var SimpleOtherObject|SimpleObject|null */
    private SimpleOtherObject|SimpleObject|null $simpleNullableObject;

    /**
     * @param SimpleObject|SimpleOtherObject      $simpleObject
     * @param SimpleObject|SimpleOtherObject|null $simpleNullableObject
     */
    public function __construct(
        SimpleObject|SimpleOtherObject $simpleObject,
        SimpleObject|SimpleOtherObject|null $simpleNullableObject
    ) {
        $this->simpleObject = $simpleObject;
        $this->simpleNullableObject = $simpleNullableObject;
    }

    /**
     * @param SimpleObject|SimpleOtherObject $simpleObject
     */
    public function setSimpleObject(SimpleObject|SimpleOtherObject $simpleObject): void
    {
        $this->simpleObject = $simpleObject;
    }

    /**
     * @param SimpleObject|SimpleOtherObject|null $simpleNullableObject
     */
    public function setSimpleNullableObject(SimpleObject|SimpleOtherObject|null $simpleNullableObject): void
    {
        $this->simpleNullableObject = $simpleNullableObject;
    }

    public function getSimpleNullableObject(): ?SimpleObject
    {
        return $this->simpleNullableObject;
    }

    public function getSimpleObject(): SimpleObject
    {
        return $this->simpleObject;
    }
}