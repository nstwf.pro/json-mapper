<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation\Inspector;


use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;


final class IsCalledInspector implements Inspector
{
    private bool $isCalled = false;

    public function isCalled(): bool
    {
        return $this->isCalled;
    }

    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        $this->isCalled = true;

        return new ObjectDescriptor();
    }
}