<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class MixedTypeObject
{
    /** @var mixed */
    private mixed $mixed;

    /**
     * @param mixed $mixed
     */
    public function __construct(mixed $mixed)
    {
        $this->mixed = $mixed;
    }

    public function getMixed(): mixed
    {
        return $this->mixed;
    }

    /**
     * @param mixed $mixed
     */
    public function setMixed(mixed $mixed): void
    {
        $this->mixed = $mixed;
    }
}