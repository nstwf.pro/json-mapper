<?php


namespace Nstwf\JsonMapper\Unit\Type;


use Nstwf\JsonMapper\Type\TypeDescriptor;
use PHPUnit\Framework\TestCase;


class TypeDescriptorTest extends TestCase
{
    public function testIsEquals()
    {
        $mainType = new TypeDescriptor('string', false);
        $anotherType = new TypeDescriptor('string', false);

        $this->assertTrue($mainType->isEquals($anotherType));
    }

    public function testIsNotEqualsByIsArray()
    {
        $mainType = new TypeDescriptor('string', false);
        $anotherType = new TypeDescriptor('string', true);

        $this->assertFalse($mainType->isEquals($anotherType));
    }

    public function testIsNotEqualsByName()
    {
        $mainType = new TypeDescriptor('string', false);
        $anotherType = new TypeDescriptor('int', false);

        $this->assertFalse($mainType->isEquals($anotherType));
    }

    public function testIsOnlyMixedNotArray()
    {
        $type = new TypeDescriptor('mixed', false);

        $this->assertTrue($type->isOnlyMixedNotArray());
    }

    public function testIsNotIsOnlyMixedNotArray()
    {
        $type = new TypeDescriptor('mixed', true);

        $this->assertFalse($type->isOnlyMixedNotArray());
    }

    public function testTypeToString()
    {
        $type = new TypeDescriptor('string', false);

        $this->assertEquals('string', (string)$type);
    }

    public function testArrayTypeToString()
    {
        $type = new TypeDescriptor('string', true);

        $this->assertEquals('array of string', (string)$type);
    }

    public function testConvertToJson()
    {
        $type = new TypeDescriptor('mixed', true);

        $this->assertEquals(
            '{"name":"mixed","isArray":true}'
            , json_encode($type)
        );
    }
}