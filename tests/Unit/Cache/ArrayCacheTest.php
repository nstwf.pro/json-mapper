<?php


namespace Nstwf\JsonMapper\Unit\Cache;


use Nstwf\JsonMapper\Cache\ArrayCache;
use PHPUnit\Framework\TestCase;


class ArrayCacheTest extends TestCase
{
    public function testSetValue()
    {
        $cache = new ArrayCache();

        $cache->set('1', 'value1');

        $this->assertTrue($cache->has('1'));
        $this->assertEquals('value1', $cache->get('1'));
    }

    public function testGetNotExistingKey()
    {
        $cache = new ArrayCache();

        $cache->set('key1', 'value1');

        $this->assertFalse($cache->has('key2'));
        $this->assertEquals('default', $cache->get('key2', 'default'));
    }

    public function testDeleteValue()
    {
        $cache = new ArrayCache();

        $cache->set('1', 'value1');
        $cache->delete('1');

        $this->assertFalse($cache->has('1'));
    }

    public function testClearMakeCacheEmpty()
    {
        $cache = new ArrayCache();

        $cache->set('key1', 'value1');
        $cache->set('key2', 'value2');
        $cache->set('key3', 'value3');

        $cache->clear();

        $this->assertFalse($cache->has('key1'));
        $this->assertFalse($cache->has('key2'));
        $this->assertFalse($cache->has('key3'));
    }

    public function testSetMultipleAddCorrectlyValuesAndGetThem()
    {
        $cache = new ArrayCache();

        $values = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ];

        $cache->setMultiple($values);

        $multiple = $cache->getMultiple(['key1', 'key2', 'key3']);

        $this->assertEquals($values, $multiple);
    }

    public function testGetMultipleWithNotExistingKey()
    {
        $cache = new ArrayCache();

        $cache->setMultiple([
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ]);

        $multiple = $cache->getMultiple(['key1', 'key2', 'key3', 'key4'], 'default');

        $this->assertEquals([
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'default',
        ], $multiple);
    }

    public function testDeleteMultipleKeysWorkCorrectly()
    {
        $cache = new ArrayCache();

        $cache->setMultiple([
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ]);

        $cache->deleteMultiple(['key1', 'key3']);

        $this->assertFalse($cache->has('key1'));
        $this->assertTrue($cache->has('key2'));
        $this->assertFalse($cache->has('key3'));
    }
}