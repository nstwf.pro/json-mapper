<?php


namespace Nstwf\JsonMapper\Unit\Registry;


use Nstwf\JsonMapper\Registry\ClassFactoryRegistry;
use PHPUnit\Framework\TestCase;


class ClassFactoryRegistryTest extends TestCase
{
    public function testCreateUnregisteredObject()
    {
        $registry = new ClassFactoryRegistry();

        $this->expectException(\RuntimeException::class);
        $registry->create('toStd', []);
    }

    public function testAddAlreadyRegisteredObject()
    {
        $registry = new ClassFactoryRegistry();

        $registry->add(
            'toStd',
            fn(mixed $data) => (object)($data)
        );

        $this->expectException(\RuntimeException::class);

        $registry->add(
            'toStd',
            fn(mixed $data) => (object)($data)
        );
    }

    public function testRegisterCallableAndCreate()
    {
        $registry = new ClassFactoryRegistry();

        $registry->add(
            'toStd',
            fn(mixed $data) => (object)($data)
        );

        $object = $registry->create('toStd', ['key' => 'value']);

        $expected = (object)['key' => 'value'];

        $this->assertEquals($expected, $object);
    }

    public function testRegisterInvokableClassAndCreate()
    {
        $registry = new ClassFactoryRegistry();

        $class = new class {
            public function __invoke(int $a)
            {
                return $a * 2;
            }
        };

        $registry->add(
            'invoke',
            $class
        );

        $this->assertTrue($registry->has('invoke'));
        $this->assertEquals(6, $registry->create('invoke', 3));
    }

    public function testWithBuiltInTypes()
    {
        $registry = new ClassFactoryRegistry();

        $this->assertTrue($registry->has(\DateTime::class));
        $this->assertEquals(
            new \DateTime('2022-03-24 13:31:01'),
            $registry->create(\DateTime::class, '2022-03-24 13:31:01')
        );

        $this->assertTrue($registry->has(\DateTimeImmutable::class));
        $this->assertEquals(
            new \DateTimeImmutable('2022-03-24 13:31:01'),
            $registry->create(\DateTimeImmutable::class, '2022-03-24 13:31:01')
        );
    }
}