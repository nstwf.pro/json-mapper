<?php


namespace Nstwf\JsonMapper\Unit\Data;


use Nstwf\JsonMapper\Data\DataMap;
use PHPUnit\Framework\TestCase;


class DataMapTest extends TestCase
{
    public function testAddDuplicates()
    {
        // Arrange
        $dataMap = new DataMap();

        // Act
        $dataMap->add('key', 3);
        $dataMap->add('key', 4);

        // Assert
        $this->assertEquals(4, $dataMap->get('key'));
    }

    public function testRemoveExistingKey()
    {
        // Arrange
        $dataMap = new DataMap();
        $dataMap->add('key', 5);

        // Act
        $dataMap->remove('key');

        // Assert
        $this->assertFalse($dataMap->has('key'));
    }

    public function testRemoveNotExistingKey()
    {
        // Arrange
        $dataMap = new DataMap();
        $dataMap->add('key', 5);

        // Act
        $dataMap->remove('key2');

        // Assert
        $this->assertEquals(5, $dataMap->get('key'));
    }
}