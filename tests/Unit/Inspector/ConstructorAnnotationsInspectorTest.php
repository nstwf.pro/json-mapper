<?php


namespace Nstwf\JsonMapper\Unit\Inspector;


use Nstwf\JsonMapper\Asserts\ObjectDescriptorAsserts;
use Nstwf\JsonMapper\Cache\NullCache;
use Nstwf\JsonMapper\Inspector\ConstructorAnnotationsInspector;
use Nstwf\JsonMapper\Inspector\ConstructorParametersInspector;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\Unit\Implementation\ArrayOfClassesObject;
use Nstwf\JsonMapper\Unit\Implementation\ArrayOfEnumObject;
use Nstwf\JsonMapper\Unit\Implementation\ArrayOfScalarTypesObject;
use Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum;
use Nstwf\JsonMapper\Unit\Implementation\Enum\StringEnum;
use Nstwf\JsonMapper\Unit\Implementation\EnumObject;
use Nstwf\JsonMapper\Unit\Implementation\MixedTypeObject;
use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleObject;
use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleOtherObject;
use Nstwf\JsonMapper\Unit\Implementation\NestedSimpleObject;
use Nstwf\JsonMapper\Unit\Implementation\NestedUnionSimpleObject;
use Nstwf\JsonMapper\Unit\Implementation\NoTypeObject;
use Nstwf\JsonMapper\Unit\Implementation\ScalarTypesObject;
use Nstwf\JsonMapper\Unit\Implementation\UnionEnumObject;
use Nstwf\JsonMapper\Unit\Implementation\UnionScalarTypesObject;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;


class ConstructorAnnotationsInspectorTest extends TestCase
{
    public function testInspectScalarTypes()
    {
        // Arrange
        $reflection = new ReflectionWrapper(ScalarTypesObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('intProperty')
            ->assertType('int', false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('stringProperty')
            ->assertType('string', false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('floatProperty')
            ->assertType('float', false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('boolProperty')
            ->assertType('bool', false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('arrayProperty')
            ->assertType('mixed', true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('nullableIntProperty')
            ->assertType('int', false)
            ->assertIsNullable(true);
    }

    public function testInspectArrayOfScalarTypes()
    {
        // Arrange
        $reflection = new ReflectionWrapper(ArrayOfScalarTypesObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('intArray')
            ->assertType('int', true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('stringArray')
            ->assertType('string', true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('floatArray')
            ->assertType('float', true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('boolArray')
            ->assertType('bool', true)
            ->assertIsNullable(false);
    }

    public function testInspectUnionScalarTypes()
    {
        // Arrange
        $reflection = new ReflectionWrapper(UnionScalarTypesObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('union')
            ->assertType('int', false)
            ->assertType('float', false)
            ->assertType('string', false)
            ->assertType('bool', false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('unionWithArray')
            ->assertType('int', false)
            ->assertType('float', false)
            ->assertType('string', false)
            ->assertType('bool', false)
            ->assertType('mixed', true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('nullableUnion')
            ->assertType('int', false)
            ->assertType('float', false)
            ->assertType('string', false)
            ->assertType('bool', false)
            ->assertIsNullable(true);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('unionArray')
            ->assertType('int', true)
            ->assertType('float', true)
            ->assertType('string', true)
            ->assertType('bool', true)
            ->assertIsNullable(false);
    }

    public function testInspectClasses()
    {
        // Arrange
        $reflection = new ReflectionWrapper(NestedSimpleObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('simpleObject')
            ->assertType(SimpleObject::class, false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('simpleNullableObject')
            ->assertType(SimpleObject::class, false)
            ->assertIsNullable(true);
    }

    public function testInspectArrayOfClasses()
    {
        // Arrange
        $reflection = new ReflectionWrapper(ArrayOfClassesObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('simpleObjects')
            ->assertType(SimpleObject::class, true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('simpleOtherObjects')
            ->assertType(SimpleOtherObject::class, true)
            ->assertIsNullable(false);
    }

    public function testInspectUnionClasses()
    {
        // Arrange
        $reflection = new ReflectionWrapper(NestedUnionSimpleObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('simpleObject')
            ->assertType(SimpleObject::class, false)
            ->assertType(SimpleOtherObject::class, false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('simpleNullableObject')
            ->assertType(SimpleObject::class, false)
            ->assertType(SimpleOtherObject::class, false)
            ->assertIsNullable(true);
    }

    public function testInspectEnums()
    {
        // Arrange
        $reflection = new ReflectionWrapper(EnumObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('enum')
            ->assertType(IntEnum::class, false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('nullableEnum')
            ->assertType(IntEnum::class, false)
            ->assertIsNullable(true);
    }

    public function testInspectArrayOfEnums()
    {
        // Arrange
        $reflection = new ReflectionWrapper(ArrayOfEnumObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('intEnums')
            ->assertType(IntEnum::class, true)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('stringEnums')
            ->assertType(StringEnum::class, true)
            ->assertIsNullable(false);
    }

    public function testInspectUnionEnums()
    {
        // Arrange
        $reflection = new ReflectionWrapper(UnionEnumObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('enum')
            ->assertType(IntEnum::class, false)
            ->assertType(StringEnum::class, false)
            ->assertIsNullable(false);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('nullableEnum')
            ->assertType(IntEnum::class, false)
            ->assertType(StringEnum::class, false)
            ->assertIsNullable(true);
    }

    public function testInspectMixed()
    {
        // Arrange
        $reflection = new ReflectionWrapper(MixedTypeObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('mixed')
            ->assertType('mixed', false)
            ->assertType('mixed', true)
            ->assertIsNullable(true);
    }

    public function testInspectNoType()
    {
        // Arrange
        $reflection = new ReflectionWrapper(NoTypeObject::class);
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertNotHasProperty('noType')
            ->assertNotHasProperty('noTypeNoDoc');
    }

    public function testInspectWithoutConstructor()
    {
        // Arrange
        $class = new class {
            public int $value;
        };

        $reflection = new ReflectionWrapper(get_class($class));
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertCount(0);
    }

    public function testInspectWithoutConstructorDocComment()
    {
        // Arrange
        $class = new class {
            public function __construct()
            {
            }
        };
        $reflection = new ReflectionWrapper(get_class($class));
        $inspector = new ConstructorAnnotationsInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertCount(0);
    }

    public function testSetCacheOnSuccessfullyInspect()
    {
        $cache = $this->createMock(CacheInterface::class);
        $cache->method('has')
            ->with(sprintf('%s-%s', ConstructorAnnotationsInspector::class, NoTypeObject::class))
            ->willReturn(false);
        $cache->expects($this->once())
            ->method('set')
            ->with(sprintf('%s-%s', ConstructorAnnotationsInspector::class, NoTypeObject::class));

        $reflection = new ReflectionWrapper(NoTypeObject::class);
        $inspector = new ConstructorAnnotationsInspector($cache);

        $inspector->handle($reflection);
    }

    public function testGetCachedValueIfItExists()
    {
        $reflection = $this->createMock(ReflectionWrapper::class);
        $reflection->method('getClassName')->willReturn('MockClass');
        $reflection->expects($this->never())->method('getReflectionClass');

        $objectDescriptor = new ObjectDescriptor();

        $cache = $this->createMock(CacheInterface::class);
        $cache->method('has')
            ->with(sprintf('%s-%s', ConstructorAnnotationsInspector::class, 'MockClass'))
            ->willReturn(true);
        $cache->method('get')
            ->with(sprintf('%s-%s', ConstructorAnnotationsInspector::class, 'MockClass'))
            ->willReturn($objectDescriptor);
        $cache->expects($this->never())
            ->method('set');

        $inspector = new ConstructorAnnotationsInspector($cache);

        $inspector->handle($reflection);
    }
}