<?php


namespace Nstwf\JsonMapper\Unit\Inspector;


use Nstwf\JsonMapper\Asserts\ObjectDescriptorAsserts;
use Nstwf\JsonMapper\Cache\NullCache;
use Nstwf\JsonMapper\Inspector\CustomNameAttributeInspector;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\Unit\Implementation\CustomNameAttributeObject;
use Nstwf\JsonMapper\Unit\Implementation\NoTypeObject;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;


class CustomPropertyAnnotationInspectorTest extends TestCase
{
    public function testInspectPropertyWithAttribute()
    {
        // Arrange
        $reflection = new ReflectionWrapper(CustomNameAttributeObject::class);
        $inspector = new CustomNameAttributeInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('property')
            ->assertCustomName('custom_name');
    }

    public function testInspectPropertyWithoutAttribute()
    {
        // Arrange
        $reflection = new ReflectionWrapper(NoTypeObject::class);
        $inspector = new CustomNameAttributeInspector(new NullCache());

        // Act
        $objectDescriptor = $inspector->handle($reflection);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertCount(0);
    }

    public function testSetCacheOnSuccessfullyInspect()
    {
        $cache = $this->createMock(CacheInterface::class);
        $cache->method('has')
            ->with(sprintf('%s-%s', CustomNameAttributeInspector::class, NoTypeObject::class))
            ->willReturn(false);
        $cache->expects($this->once())
            ->method('set')
            ->with(sprintf('%s-%s', CustomNameAttributeInspector::class, NoTypeObject::class));

        $reflection = new ReflectionWrapper(NoTypeObject::class);
        $inspector = new CustomNameAttributeInspector($cache);

        $inspector->handle($reflection);
    }

    public function testGetCachedValueIfItExists()
    {
        $reflection = $this->createMock(ReflectionWrapper::class);
        $reflection->method('getClassName')->willReturn('MockClass');
        $reflection->expects($this->never())->method('getReflectionClass');

        $objectDescriptor = new ObjectDescriptor();

        $cache = $this->createMock(CacheInterface::class);
        $cache->method('has')
            ->with(sprintf('%s-%s', CustomNameAttributeInspector::class, 'MockClass'))
            ->willReturn(true);
        $cache->method('get')
            ->with(sprintf('%s-%s', CustomNameAttributeInspector::class, 'MockClass'))
            ->willReturn($objectDescriptor);
        $cache->expects($this->never())
            ->method('set');

        $inspector = new CustomNameAttributeInspector($cache);

        $inspector->handle($reflection);
    }
}