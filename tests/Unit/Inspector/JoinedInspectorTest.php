<?php


namespace Nstwf\JsonMapper\Unit\Inspector;


use Nstwf\JsonMapper\Asserts\ObjectDescriptorAsserts;
use Nstwf\JsonMapper\Cache\NullCache;
use Nstwf\JsonMapper\Inspector\ConstructorParametersInspector;
use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Inspector\JoinedInspector;
use Nstwf\JsonMapper\Inspector\TypedPropertiesInspector;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Object\ObjectDescriptorBuilder;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Unit\Implementation\Inspector\IsCalledInspector;
use Nstwf\JsonMapper\Unit\Implementation\NoTypeObject;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;


class JoinedInspectorTest extends TestCase
{
    public function testIsCalledAllInspectors()
    {
        $inspectors = [
            new IsCalledInspector(),
            new IsCalledInspector(),
            new IsCalledInspector(),
        ];

        $joinedInspector = new JoinedInspector(new NullCache(), ...$inspectors);

        $joinedInspector->handle(new ReflectionWrapper(__CLASS__));

        foreach ($inspectors as $inspector) {
            $this->assertTrue($inspector->isCalled());
        }
    }

    public function testMergeAllInspectorsDescriptors()
    {
        // Arrange
        $firstInspector = $this->createMock(Inspector::class);
        $firstInspector->method('handle')
            ->willReturn(
                (new ObjectDescriptorBuilder())
                    ->addProperty(
                        (new PropertyDescriptorBuilder())
                            ->addType(new TypeDescriptor('int', false))
                            ->setName('int')
                            ->setIsNullable(false)
                            ->build()
                    )
                    ->build()
            );

        $secondInspector = $this->createMock(Inspector::class);
        $secondInspector->method('handle')
            ->willReturn(
                (new ObjectDescriptorBuilder())
                    ->addProperty(
                        (new PropertyDescriptorBuilder())
                            ->addType(new TypeDescriptor('float', true))
                            ->setName('float')
                            ->setIsNullable(true)
                            ->build()
                    )
                    ->build()
            );

        $joinedInspector = new JoinedInspector(new NullCache(), $firstInspector, $secondInspector);

        $reflectionWrapper = $this->createMock(ReflectionWrapper::class);

        // Act
        $objectDescriptor = $joinedInspector->handle($reflectionWrapper);

        // Assert
        $objectDescriptorAsserts = new ObjectDescriptorAsserts($objectDescriptor);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('float')
            ->assertType('float', true)
            ->assertIsNullable(true);

        $objectDescriptorAsserts->propertyMapAsserts()
            ->assertProperty('int')
            ->assertType('int', false)
            ->assertIsNullable(false);
    }

    public function testSetCacheOnSuccessfullyInspect()
    {
        $cache = $this->createMock(CacheInterface::class);
        $cache->method('has')
            ->with(sprintf('%s-%s', JoinedInspector::class, NoTypeObject::class))
            ->willReturn(false);
        $cache->expects($this->once())
            ->method('set')
            ->with(sprintf('%s-%s', JoinedInspector::class, NoTypeObject::class));

        $reflection = new ReflectionWrapper(NoTypeObject::class);
        $inspector = new JoinedInspector($cache, new IsCalledInspector());

        $inspector->handle($reflection);
    }

    public function testGetCachedValueIfItExists()
    {
        $objectDescriptor = new ObjectDescriptor();

        $cache = $this->createMock(CacheInterface::class);
        $cache->method('has')
            ->with(sprintf('%s-%s', JoinedInspector::class, NoTypeObject::class))
            ->willReturn(true);
        $cache->method('get')
            ->with(sprintf('%s-%s', JoinedInspector::class, NoTypeObject::class))
            ->willReturn($objectDescriptor);
        $cache->expects($this->never())
            ->method('set');

        $inspector = new JoinedInspector($cache, new IsCalledInspector());

        $reflection = new ReflectionWrapper(NoTypeObject::class);
        $inspector->handle($reflection);
    }
}