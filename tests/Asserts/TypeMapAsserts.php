<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Asserts;


use Nstwf\JsonMapper\Type\TypeMap;
use PHPUnit\Framework\Assert;


final class TypeMapAsserts
{
    public function __construct(
        private TypeMap $typeMap
    ) {
    }

    public function assertTypeExists(string $typeName, bool $isArray): self
    {
        Assert::assertTrue($this->typeMap->has($typeName, $isArray));

        return $this;
    }

    public function assertTypeNotExists(string $typeName, bool $isArray = null): self
    {
        if ($isArray === null) {
            Assert::assertFalse($this->typeMap->has($typeName, true));
            Assert::assertFalse($this->typeMap->has($typeName, false));
        } else {
            Assert::assertFalse($this->typeMap->has($typeName, $isArray));
        }

        return $this;
    }
}