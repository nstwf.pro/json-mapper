<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Asserts;


use Nstwf\JsonMapper\Property\PropertyMap;
use PHPUnit\Framework\Assert;


final class PropertyMapAsserts
{
    public function __construct(
        private PropertyMap $propertyMap
    ) {
    }

    public function assertProperty(string $propertyName): PropertyDescriptorAsserts
    {
        $this->assertHasProperty($propertyName);

        $property = $this->propertyMap->get($propertyName);

        return new PropertyDescriptorAsserts($property);
    }

    public function assertHasProperty(string $propertyName): self
    {
        Assert::assertTrue($this->propertyMap->has($propertyName));

        return $this;
    }

    public function assertNotHasProperty(string $propertyName): self
    {
        Assert::assertFalse($this->propertyMap->has($propertyName));

        return $this;
    }

    public function assertCount(int $count): self
    {
        Assert::assertCount($count, $this->propertyMap);

        return $this;
    }
}