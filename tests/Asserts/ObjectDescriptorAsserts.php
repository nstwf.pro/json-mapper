<?php


namespace Nstwf\JsonMapper\Asserts;


use Nstwf\JsonMapper\Object\ObjectDescriptor;


final class ObjectDescriptorAsserts
{
    public function __construct(
        private ObjectDescriptor $objectDescriptor
    ) {
    }

    public function propertyMapAsserts(): PropertyMapAsserts
    {
        return new PropertyMapAsserts($this->objectDescriptor->getPropertyMap());
    }
}