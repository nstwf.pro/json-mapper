<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Asserts;


use Nstwf\JsonMapper\Property\PropertyDescriptor;
use PHPUnit\Framework\Assert;


final class PropertyDescriptorAsserts
{
    public function __construct(
        private PropertyDescriptor $property
    ) {
    }

    public function assertType(string $name, bool $isArray): self
    {
        $typeMapAsserts = new TypeMapAsserts($this->property->getTypes());

        $typeMapAsserts->assertTypeExists($name, $isArray);

        return $this;
    }

    public function assertIsNullable(bool $isNullable): self
    {
        Assert::assertEquals($isNullable, $this->property->isNullable());

        return $this;
    }

    public function assertCustomName(?string $customName): self
    {
        Assert::assertEquals($customName, $this->property->getCustomName());

        return $this;
    }
}