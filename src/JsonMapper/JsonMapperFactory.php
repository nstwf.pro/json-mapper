<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\JsonMapper;


use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Inspector\InspectorBuilder;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;


final class JsonMapperFactory
{
    public function __construct(
        private JsonMapperBuilder $jsonMapperBuilder = new JsonMapperBuilder()
    ) {
    }

    public function create(Inspector $inspector = null, ObjectMapper $objectBuilder = null): JsonMapper
    {
        return clone ($this->jsonMapperBuilder)
            ->withObjectMapper($objectBuilder ?? $this->defaultObjectBuilder())
            ->withInspector($inspector ?? $this->defaultInspector())
            ->build();
    }

    private function defaultObjectBuilder(): ObjectMapper
    {
        return new ObjectMapper();
    }

    private function defaultInspector(): Inspector
    {
        return (new InspectorBuilder())
            ->withConstructorAnnotations()
            ->withConstructorParameters()
            ->withPropertyAnnotations()
            ->withTypesProperties()
            ->build();
    }
}