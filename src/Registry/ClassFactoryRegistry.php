<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Registry;


use RuntimeException;


final class ClassFactoryRegistry
{
    /** @var array<int, callable> */
    private array $registry = [];

    public function __construct()
    {
        return $this
            ->add(\DateTime::class, fn(mixed $data) => new \DateTime($data))
            ->add(\DateTimeImmutable::class, fn(mixed $data) => new \DateTimeImmutable($data));
    }

    public function add(string $className, callable $factoryMethod): self
    {
        if ($this->has($className)) {
            throw new RuntimeException();
        }

        $this->registry[$className] = $factoryMethod;

        return $this;
    }

    public function has(string $className): bool
    {
        return array_key_exists($className, $this->registry);
    }

    public function create(string $className, mixed $data): mixed
    {
        if ($this->has($className)) {
            $factoryMethod = $this->registry[$className];

            return $factoryMethod($data);
        }

        throw new RuntimeException();
    }
}