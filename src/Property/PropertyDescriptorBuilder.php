<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Property;


use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;


/**
 * TODO: add unit tests
 */
final class PropertyDescriptorBuilder
{
    private string $name;
    private TypeMap $types;
    private bool $isNullable = false;
    private ?string $customName = null;

    public function __construct()
    {
        $this->types = new TypeMap();
    }

    public function build(): PropertyDescriptor
    {
        return new PropertyDescriptor(
            $this->name,
            $this->types,
            $this->isNullable,
            $this->customName
        );
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setTypes(TypeMap $types): self
    {
        $this->types = $types;
        return $this;
    }

    public function addType(TypeDescriptor $type): self
    {
        $this->types->add($type);
        return $this;
    }

    public function setIsNullable(bool $isNullable): self
    {
        $this->isNullable = $isNullable;
        return $this;
    }

    public function setCustomName(string $customName): self
    {
        $this->customName = $customName;
        return $this;
    }
}