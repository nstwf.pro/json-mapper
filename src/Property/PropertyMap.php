<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Property;


final class PropertyMap implements \Countable, \JsonSerializable
{
    /** @var PropertyDescriptor[] */
    private array $properties = [];

    public function add(PropertyDescriptor $propertyDescriptor): self
    {
        $this->properties[$propertyDescriptor->getName()] = $propertyDescriptor;

        return $this;
    }

    public function get(string $propertyName): PropertyDescriptor
    {
        return $this->properties[$propertyName];
    }

    public function has(string $propertyName): bool
    {
        return array_key_exists($propertyName, $this->properties);
    }

    public function merge(self $self): self
    {
        $clone = clone($this);

        foreach ($self->properties as $anotherPropertyDescriptor) {
            $propertyName = $anotherPropertyDescriptor->getName();

            if ($this->has($propertyName)) {
                $propertyDescriptor = $this->get($propertyName);

                if (!$propertyDescriptor->isEquals($anotherPropertyDescriptor)) {
                    $clone->add($propertyDescriptor->merge($anotherPropertyDescriptor));
                }
            } else {
                $clone->add($anotherPropertyDescriptor);
            }
        }

        return $clone;
    }

    public function count(): int
    {
        return count($this->properties);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}