<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Property\Mapper;


use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\OperationResult\OperationResult;
use Nstwf\JsonMapper\Registry\ClassFactoryRegistry;
use Nstwf\JsonMapper\ScalarTypes\ScalarType;
use Nstwf\JsonMapper\ScalarTypes\ScalarTypeCast;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;


class PropertyMapper
{
    public function __construct(
        private ClassFactoryRegistry $registry = new ClassFactoryRegistry()
    ) {
    }

    public function mapValue(JsonMapper $jsonMapper, TypeMap $typeMap, mixed $value): OperationResult
    {
        if ($typeMap->isUnion()) {
            return $this->mapUnionType($jsonMapper, $typeMap, $value);
        } elseif (!$typeMap->isEmpty()) {
            return $this->mapType($jsonMapper, $typeMap->first(), $value);
        } else {
            return OperationResult::success($value);
        }
    }

    private function mapUnionType(JsonMapper $jsonMapper, TypeMap $typeMap, mixed $value): OperationResult
    {
        foreach ($typeMap->all() as $typeDescriptor) {
            $mapTypeResult = $this->mapType($jsonMapper, $typeDescriptor, $value);

            if ($mapTypeResult->isSuccess()) {
                return $mapTypeResult;
            }
        }

        return OperationResult::error(
            sprintf("Can not map data (%s) to union type (%s)", json_encode($value), $typeMap)
        );
    }

    private function mapType(JsonMapper $jsonMapper, TypeDescriptor $type, mixed $value): OperationResult
    {
        $typeName = $type->getName();

        if (is_array($value) ^ $type->isArray()) {
            return OperationResult::error(
                sprintf('Defined type is %s, but value type is %s', $type->isArray() ? 'array' : 'not array', is_array($value) ? 'array' : 'not array')
            );
        }

        if ($type->isMixed()) {
            return OperationResult::success($value);
        }

        if ($scalarType = ScalarType::tryFrom($typeName)) {
            if ($type->isArray()) {
                return $this->mapToArrayOfScalarTypes($scalarType, $value);
            } else {
                return $this->mapToScalarValue($scalarType, $value);
            }
        }

        if ($this->registry->has($typeName)) {
            if ($type->isArray()) {
                return $this->createArrayFromRegistry($typeName, $value);
            } else {
                return $this->createFromRegistry($typeName, $value);
            }
        }

        if (\enum_exists($typeName)) {
            $scalarType = $this->enumBackedScalarType($typeName);

            if ($type->isArray()) {
                return $this->mapToArrayOfEnums($typeName, $scalarType, $value);
            } else {
                return $this->mapToEnum($typeName, $scalarType, $value);
            }
        }

        if (\class_exists($typeName)) {
            if ($type->isArray()) {
                return $this->mapToArrayOfObjects($jsonMapper, $typeName, $value);
            } else {
                return $this->mapToObject($jsonMapper, $typeName, $value);
            }
        }

        return OperationResult::error(
            sprintf("Can not map data (%s) to type (%s)", json_encode($value), $typeName)
        );
    }

    private function mapToArrayOfScalarTypes(ScalarType $scalarType, array $values): OperationResult
    {
        $data = [];

        foreach ($values as $value) {
            $mapResult = $this->mapToScalarValue($scalarType, $value);

            if (!$mapResult->isSuccess()) {
                return $mapResult;
            }

            $data[] = $mapResult->value();
        }

        return OperationResult::success($data);
    }

    private function mapToScalarValue(ScalarType $scalarType, mixed $value): OperationResult
    {
        $valueType = \gettype_custom($value);

        if ($valueType !== $scalarType->value) {
            return OperationResult::error(
                sprintf("Value type (%s) is not equals defined type (%s)", $valueType, $scalarType->value)
            );
        }

        return OperationResult::success($this->castToScalarType($scalarType, $value));
    }

    private function mapToArrayOfEnums(\BackedEnum|string $typeName, ScalarType $scalarType, array $values): OperationResult
    {
        $data = [];

        foreach ($values as $value) {
            $mapResult = $this->mapToEnum($typeName, $scalarType, $value);

            if (!$mapResult->isSuccess()) {
                return $mapResult;
            }

            $data[] = $mapResult->value();
        }

        return OperationResult::success($data);
    }

    private function mapToEnum(\BackedEnum|string $typeName, ScalarType $scalarType, mixed $value): OperationResult
    {
        $enumValue = $this->castToScalarType($scalarType, $value);

        if ($enum = $typeName::tryFrom($enumValue)) {
            return OperationResult::success($enum);
        }

        return OperationResult::error(sprintf('Cannot map value (%s) to enum (%s)', json_encode($value), $typeName));
    }

    private function mapToArrayOfObjects(JsonMapper $jsonMapper, string $className, array $values): OperationResult
    {
        $data = [];

        foreach ($values as $value) {
            $registryResult = $this->mapToObject($jsonMapper, $className, $value);

            if ($registryResult->isError()) {
                return $registryResult;
            }

            $data[] = $registryResult->value();
        }

        return OperationResult::success($data);
    }

    private function mapToObject(JsonMapper $jsonMapper, string $className, mixed $value): OperationResult
    {
        return $jsonMapper->mapObject($value, $className);
    }

    private function castToScalarType(ScalarType $scalarType, mixed $value): mixed
    {
        return ScalarTypeCast::cast($scalarType, $value);
    }

    private function createArrayFromRegistry(string $typeName, array $values): OperationResult
    {
        $results = [];

        foreach ($values as $value) {
            $registryResult = $this->createFromRegistry($typeName, $value);

            if ($registryResult->isError()) {
                return $registryResult;
            }

            $results[] = $registryResult->value();
        }

        return OperationResult::success($results);
    }

    private function createFromRegistry(string $typeName, mixed $value): OperationResult
    {
        try {
            $created = $this->registry->create($typeName, $value);

            if (interface_exists($typeName)) {
                if (!is_subclass_of($created, $typeName)) {
                    return OperationResult::error(
                        sprintf('Registry created object type (%s), must implements defined interface (%s)', get_class($created), $typeName)
                    );
                }
            } elseif (get_class($created) !== $typeName) {
                return OperationResult::error(
                    sprintf('Registry created object type - %s, but defined - %s', gettype_custom($created), $typeName)
                );
            }

            return OperationResult::success($created);
        } catch (\Exception $ex) {
            return OperationResult::error(
                sprintf('Can not create object (%s) from registry with data (%s). Error: %s', $typeName, json_encode($value), $ex->getMessage())
            );
        }
    }

    private function enumBackedScalarType(string $typeName): ScalarType
    {
        $reflectionEnum = new \ReflectionEnum($typeName);

        return ScalarType::from((string)$reflectionEnum->getBackingType());
    }
}