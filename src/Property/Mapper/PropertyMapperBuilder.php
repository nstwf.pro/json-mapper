<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Property\Mapper;


use Nstwf\JsonMapper\Registry\ClassFactoryRegistry;


final class PropertyMapperBuilder
{
    private ClassFactoryRegistry $registry;

    public function build(): PropertyMapper
    {
        return new PropertyMapper(
            $this->registry,
        );
    }

    public function withClassFactoryRegistry(ClassFactoryRegistry $registry): self
    {
        $this->registry = $registry;

        return $this;
    }
}