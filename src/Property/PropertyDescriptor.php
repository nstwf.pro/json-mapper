<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Property;


use Nstwf\JsonMapper\Type\TypeMap;


final class PropertyDescriptor implements \JsonSerializable
{
    public function __construct(
        private string $name,
        private TypeMap $types,
        private bool $isNullable,
        private ?string $customName
    ) {
    }

    public function merge(self $self): self
    {
        return new self(
            $this->name,
            $this->types->merge($self->types),
            $this->isNullable || $self->isNullable,
            $this->customName ?? $self->customName
        );
    }

    public function isEquals(self $self): bool
    {
        return $this->name === $self->name
            && $this->isNullable === $self->isNullable
            && $this->types->isEquals($self->types);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTypes(): TypeMap
    {
        return $this->types;
    }

    public function isNullable(): bool
    {
        return $this->isNullable;
    }

    public function getCustomName(): ?string
    {
        return $this->customName;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}