<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Object;


use Nstwf\JsonMapper\Property\PropertyMap;


/**
 * TODO: add unit tests
 */
final class ObjectDescriptor implements \JsonSerializable
{
    public function __construct(
        private PropertyMap $propertyMap = new PropertyMap()
    ) {
    }

    public function merge(self $self): self
    {
        return new self(
            $this->propertyMap->merge($self->propertyMap)
        );
    }

    public function getPropertyMap(): PropertyMap
    {
        return $this->propertyMap;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}