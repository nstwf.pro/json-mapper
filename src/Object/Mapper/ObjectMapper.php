<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Object\Mapper;


use Nstwf\JsonMapper\Data\DataMap;
use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\OperationResult\OperationResult;
use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;


class ObjectMapper
{
    public function __construct(
        private PropertyMapper $propertyMapper = new PropertyMapper()
    ) {
    }

    public function map(JsonMapper $jsonMapper, ReflectionWrapper $reflectionWrapper, ObjectDescriptor $objectDescriptor, DataMap $dataMap): OperationResult
    {
        return $this->createInstance($jsonMapper, $reflectionWrapper, $objectDescriptor, $dataMap);
    }

    private function createInstance(JsonMapper $jsonMapper, ReflectionWrapper $reflectionWrapper, ObjectDescriptor $objectDescriptor, DataMap $dataMap): OperationResult
    {
        $propertiesResult = $this->constructorParameters($jsonMapper, $reflectionWrapper, $objectDescriptor, $dataMap);

        if (!$propertiesResult->isSuccess()) {
            return $propertiesResult;
        }

        return OperationResult::success($reflectionWrapper->getReflectionClass()->newInstanceArgs($propertiesResult->value()));
    }

    private function constructorParameters(JsonMapper $jsonMapper, ReflectionWrapper $reflectionWrapper, ObjectDescriptor $objectDescriptor, DataMap $dataMap): OperationResult
    {
        $reflectionClass = $reflectionWrapper->getReflectionClass();
        $constructor = $reflectionClass->getConstructor();

        $propertyMap = $objectDescriptor->getPropertyMap();

        if ($constructor === null) {
            return OperationResult::success([]);
        }

        if (!$reflectionClass->isInstantiable()) {
            return OperationResult::error("Class {$reflectionWrapper->getClassName()} is not instantiable");
        }

        $parameters = [];

        foreach ($constructor->getParameters() as $parameter) {
            $parameterName = $parameter->getName();

            if ($dataMap->has($parameterName)) {
                $property = $propertyMap->get($parameterName);
                $value = $dataMap->get($parameterName);

                if ($property->isNullable() && $value === null) {
                    $parameters[$parameterName] = null;
                } elseif (!$property->isNullable() && $value === null) {
                    return OperationResult::error(
                        sprintf("Constructor parameter (%s) for class (%s) can not be null", $parameter->getName(), $reflectionWrapper->getClassName())
                    );
                } else {
                    $mapValueResult = $this->propertyMapper->mapValue($jsonMapper, $property->getTypes(), $value);

                    if ($mapValueResult->isError()) {
                        return OperationResult::error(
                            sprintf('Constructor parameter (%s) for class (%s): %s', $property->getName(), $reflectionClass->getName(), $mapValueResult->errorMessage())
                        );
                    }

                    $parameters[$parameterName] = $mapValueResult->value();
                }

                $dataMap->remove($parameterName);
            } elseif ($parameter->isDefaultValueAvailable()) {
                $parameters[$parameterName] = $parameter->getDefaultValue();
            } else {
                return OperationResult::error(
                    sprintf("Constructor parameter (%s) for class (%s) is required and does not exist", $parameter->getName(), $reflectionWrapper->getClassName())
                );
            }
        }

        return OperationResult::success($parameters);
    }
}