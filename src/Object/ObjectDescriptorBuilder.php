<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Object;


use Nstwf\JsonMapper\Property\PropertyDescriptor;
use Nstwf\JsonMapper\Property\PropertyMap;

/**
 * TODO: add unit tests
 */
final class ObjectDescriptorBuilder
{
    private PropertyMap $propertyMap;

    public function __construct()
    {
        $this->propertyMap = new PropertyMap();
    }

    public function build(): ObjectDescriptor
    {
        return new ObjectDescriptor(
            $this->propertyMap
        );
    }

    public function addProperty(PropertyDescriptor $property): self
    {
        $this->propertyMap->add($property);

        return $this;
    }
}