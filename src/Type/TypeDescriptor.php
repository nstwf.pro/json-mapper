<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Type;


final class TypeDescriptor implements \JsonSerializable
{
    public function __construct(
        private string $name,
        private bool $isArray,
    ) {
    }

    public function isEquals(self $self): bool
    {
        return $this->name === $self->name && $this->isArray === $self->isArray;
    }

    public function isMixed(): bool
    {
        return $this->name === 'mixed';
    }

    public function isOnlyMixedNotArray(): bool
    {
        return $this->name === 'mixed' && !$this->isArray;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isArray(): bool
    {
        return $this->isArray;
    }

    public function __toString(): string
    {
        if ($this->isArray) {
            return "array of {$this->name}";
        }

        return $this->name;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}