<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Cache\ArrayCache;
use Psr\SimpleCache\CacheInterface;


final class InspectorBuilder
{
    /** @var Inspector[] */
    private array $inspectors = [];

    private CacheInterface $cache;

    public function __construct()
    {
        $this->withCache(new ArrayCache());
    }

    public function build(): Inspector
    {
        if (empty($this->inspectors)) {
            throw new \RuntimeException();
        }

        return new JoinedInspector($this->cache, ...$this->inspectors);
    }

    public function addInspector(Inspector $inspector)
    {
        $this->inspectors[$inspector::class] = $inspector;
    }

    public function withCache(CacheInterface $cache): self
    {
        $this->cache = $cache;

        return $this;
    }

    public function withPropertyAnnotations(): self
    {
        $this->addInspector(new PropertyAnnotationsInspector($this->cache));

        return $this;
    }

    public function withTypesProperties(): self
    {
        $this->addInspector(new TypedPropertiesInspector($this->cache));

        return $this;
    }

    public function withConstructorAnnotations(): self
    {
        $this->addInspector(new ConstructorAnnotationsInspector($this->cache));

        return $this;
    }

    public function withConstructorParameters(): self
    {
        $this->addInspector(new ConstructorParametersInspector($this->cache));

        return $this;
    }

    public function withCustomNameAttribute(): self
    {
        $this->addInspector(new CustomNameAttributeInspector($this->cache));

        return $this;
    }
}