<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Object\ObjectDescriptorBuilder;
use Nstwf\JsonMapper\PhpDoc\PhpDocParser;
use Nstwf\JsonMapper\Property\PropertyDescriptor;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\ScalarTypes\ScalarType;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Psr\SimpleCache\CacheInterface;
use ReflectionProperty;


final class ConstructorAnnotationsInspector implements Inspector
{
    public function __construct(
        private CacheInterface $cache
    ) {
    }

    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        $cacheKey = sprintf("%s-%s", __CLASS__, $wrapper->getClassName());

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $builder = new ObjectDescriptorBuilder();

        $constructor = $wrapper->getReflectionClass()->getConstructor();

        if ($constructor === null) {
            return $builder->build();
        }

        $docComment = $constructor->getDocComment();

        if (!$docComment) {
            return $builder->build();
        }

        foreach ($constructor->getParameters() as $parameter) {
            $parameterName = $parameter->getName();

            $annotation = PhpDocParser::parseParamAnnotation($docComment, $parameterName);

            if ($annotation) {
                $property = $this->buildPropertyFromAnnotation($wrapper, $parameterName, $annotation);
                $builder->addProperty($property);
            }
        }

        $objectDescriptor = $builder->build();

        $this->cache->set($cacheKey, $objectDescriptor);

        return $objectDescriptor;
    }

    private function buildPropertyFromAnnotation(ReflectionWrapper $reflectionWrapper, string $propertyName, string $annotation): PropertyDescriptor
    {
        $typeNames = explode('|', $annotation);
        $isNullable = in_array('null', $typeNames, true) || in_array('mixed', $typeNames, true);

        $builder = (new PropertyDescriptorBuilder())
            ->setName($propertyName)
            ->setIsNullable($isNullable);

        foreach ($typeNames as $typeName) {
            if ($typeName === 'null') {
                continue;
            }

            $typeName = trim($typeName);

            if ($typeName === 'array') {
                $builder->addType(new TypeDescriptor('mixed', true));
            } elseif ($typeName === 'mixed') {
                $builder->addType(new TypeDescriptor('mixed', false));
                $builder->addType(new TypeDescriptor('mixed', true));
            } else {
                $isArray = str_ends_with($typeName, '[]');

                if ($isArray) {
                    $typeName = substr($typeName, 0, -2);
                }

                if (ScalarType::isScalar($typeName)) {
                    $builder->addType(new TypeDescriptor($typeName, $isArray));
                } else {
                    $reflectionClass = $reflectionWrapper->getReflectionClass();
                    $useStatementMap = $reflectionClass->getUseStatementsMap();

                    if ($useStatementMap->hasUse($typeName)) {
                        $className = $useStatementMap->get($typeName)->getClass();
                        $builder->addType(new TypeDescriptor($className, $isArray));
                    } else {
                        $builder->addType(new TypeDescriptor($reflectionClass->getNamespaceName() . '\\' . $typeName, $isArray));
                    }
                }
            }
        }

        return $builder->build();
    }
}