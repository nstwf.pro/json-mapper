<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Object\ObjectDescriptorBuilder;
use Nstwf\JsonMapper\Property\PropertyDescriptor;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;
use Psr\SimpleCache\CacheInterface;
use ReflectionNamedType;
use ReflectionProperty;
use ReflectionUnionType;


final class TypedPropertiesInspector implements Inspector
{
    public function __construct(
        private CacheInterface $cache
    ) {
    }

    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        $cacheKey = sprintf("%s-%s", __CLASS__, $wrapper->getClassName());

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $builder = new ObjectDescriptorBuilder();

        $properties = $wrapper->getReflectionClass()->getProperties();

        foreach ($properties as $property) {
            $property = $this->buildProperty($property);
            $builder->addProperty($property);
        }

        $objectDescriptor = $builder->build();

        $this->cache->set($cacheKey, $objectDescriptor);

        return $objectDescriptor;
    }

    private function buildProperty(ReflectionProperty $property): PropertyDescriptor
    {
        $parameterType = $property->getType();

        if ($parameterType instanceof ReflectionNamedType) {
            return $this->namedType($property);
        }

        if ($parameterType instanceof ReflectionUnionType) {
            return $this->unionType($property);
        }

        return $this->undefinedType($property);
    }

    private function namedType(ReflectionProperty $property): PropertyDescriptor
    {
        $propertyName = $property->getName();
        $propertyType = $property->getType();
        $typeName = $propertyType->getName();

        $types = [];

        if ($typeName == 'array') {
            $types[] = new TypeDescriptor('mixed', true);
        } elseif ($typeName == 'mixed') {
            $types[] = new TypeDescriptor('mixed', false);
            $types[] = new TypeDescriptor('mixed', true);
        } else {
            $types[] = new TypeDescriptor($typeName, false);
        }

        return (new PropertyDescriptorBuilder())
            ->setName($propertyName)
            ->setIsNullable($propertyType->allowsNull())
            ->setTypes(new TypeMap(...$types))
            ->build();
    }

    private function unionType(ReflectionProperty $property): PropertyDescriptor
    {
        $types = [];

        $typeNames = array_map(
            fn(ReflectionNamedType $namedType) => $namedType->getName(),
            $property->getType()->getTypes()
        );

        foreach ($typeNames as $typeName) {
            if ($typeName === 'array') {
                $types[] = new TypeDescriptor('mixed', true);
            } else {
                $types[] = new TypeDescriptor($typeName, false);
            }
        }

        return (new PropertyDescriptorBuilder())
            ->setName($property->getName())
            ->setIsNullable($property->getType()->allowsNull())
            ->setTypes(new TypeMap(...$types))
            ->build();
    }

    private function undefinedType(ReflectionProperty $property): PropertyDescriptor
    {
        return (new PropertyDescriptorBuilder())
            ->setName($property->getName())
            ->setIsNullable(true)
            ->setTypes(new TypeMap(
                new TypeDescriptor('mixed', false),
                new TypeDescriptor('mixed', true),
            ))
            ->build();
    }
}